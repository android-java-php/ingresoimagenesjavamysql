package org.imagenesmysqljava.basededatos;

/**
 *
 * @author Josue Daniel Roldan Ochoa

*/

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;
public class Conexion {
    public String db="baratitos";
    public String url="jdbc:mysql://localhost/"+db;
    public String user= "root";
    public String pass="";
    public Conexion(){}
    
    public Connection conectar(){
    Connection link= null;
    try{
    Class.forName("com.mysql.jdbc.Driver");
    link= DriverManager.getConnection(this.url,this.user, this.pass);
    }catch(Exception e){
    JOptionPane.showMessageDialog(null,e);
    }
    return link;
    }
}
