package org.imagenesmysqljava.panelprincipal;

import com.mysql.jdbc.Connection;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.imagenesmysqljava.basededatos.Conexion;
import org.imagenesmysqljava.info.panelinfo;

/**
 *EDUCARE.INC
 * @author Trucos Jen Edit.By Josue Roldan
 * Contact Jen: jenjen172009@gmail.com
 */


public class Form1 extends javax.swing.JFrame {
     FileInputStream fis;
     int longitudBytes;
  
     Integer ss;
    public Form1() {
        initComponents();
      //  mostrardatos();
        this.setLocationRelativeTo(null);
        this.setBackground(Color.white);
        fecha();
        cancelar();
        img();
        lblfoto.setEnabled(true);
        txnmid.setVisible(false);
        txaño.setVisible(false);
        txmes.setVisible(false);
        txdia.setVisible(false);
    }
    
    public void nuevo(){
    img();
    btnAgregarImagen.setEnabled(true);
    txtcodigo.setText("");
    txtnombre.setText("");
 
    txtcodigo.setEnabled(true);
    txtnombre.setEnabled(true);
    lblfoto.setEnabled(true);
    btnGuardar.setEnabled(true);
    btmodificar.setEnabled(false);
    txnmid.setText("");
    }
    
    public void cancelar(){
       
    btnAgregarImagen.setEnabled(false);
    txtcodigo.setText("");
    txtnombre.setText("");
    btnGuardar.setEnabled(false);
    txtcodigo.setEnabled(false);
    txtnombre.setEnabled(false);
    lblfoto.setEnabled(false);
    btmodificar.setEnabled(false);
    txnmid.setText("");
 
    }

    public void mostrardatos(){
    DefaultTableModel modelo= new DefaultTableModel();
    modelo.addColumn("ID");
    modelo.addColumn("ALTO");
    modelo.addColumn("ANCHO");
    modelo.addColumn("TIPO");
    String sSql= "SELECT id,anchura,altura,tipo FROM imagephp;";
     
    Conexion mysql= new Conexion();
    Connection cn = (Connection) mysql.conectar();
     
    try{
        String[] array= new String[6];
      
    Statement st= cn.createStatement();
    ResultSet rt= st.executeQuery(sSql);
    while(rt.next()){
     array[0]= rt.getString(1);
      array[1]=  rt.getString(2);
      array[2]=  rt.getString(3);
      array[3]=  rt.getString(4);
     
      
    modelo.addRow(array);
   
    }
     st.close();
    }catch(Exception e){JOptionPane.showMessageDialog(null,e);}
    tbl1.setModel(modelo);
    
    }
    
    
   public void buscarcodigo(String codigo){
    DefaultTableModel modelo= new DefaultTableModel();
    modelo.addColumn("ID");
    modelo.addColumn("ALTO");
    modelo.addColumn("ANCHO");
    modelo.addColumn("TIPO");
    String sSql= "SELECT id,anchura,altura,tipo FROM imagephp WHERE codigo="+codigo+";";
     
    Conexion mysql= new Conexion();
    Connection cn = (Connection) mysql.conectar();
     
    try{
        String[] array= new String[6];
      
    Statement st= cn.createStatement();
    ResultSet rt= st.executeQuery(sSql);
    while(rt.next()){
     array[0]= rt.getString(1);
     array[1]=  rt.getString(2);
     array[2]=  rt.getString(3);
     array[3]=  rt.getString(4);
     modelo.addRow(array);
   
    }
     st.close();
    }catch(Exception e){JOptionPane.showMessageDialog(null,e);}
    tbl1.setModel(modelo);
    
    }
    
    
    public void buscarnombre(String nombre){
    DefaultTableModel modelo= new DefaultTableModel();
    modelo.addColumn("ID");
    modelo.addColumn("CODIGO");
    modelo.addColumn("NOMBRE");
    modelo.addColumn("INGDIA");
    modelo.addColumn("INGMES");
    modelo.addColumn("INGAÑO");
    String sSql= "SELECT id,codigo,nombre,dia,mes,año FROM imagenes WHERE nombre="+nombre;
     
    Conexion mysql= new Conexion();
    Connection cn = (Connection) mysql.conectar();
     
    try{
        String[] array= new String[6];
      
    Statement st= cn.createStatement();
    ResultSet rt= st.executeQuery(sSql);
    while(rt.next()){
     array[0]= rt.getString(1);
     array[1]=  rt.getString(2);
     array[2]=  rt.getString(3);
     array[3]=  rt.getString(4);
     array[4]=  rt.getString(5);
     array[5]=  rt.getString(6);
     modelo.addRow(array);
   
    }
     st.close();
    }catch(Exception e){JOptionPane.showMessageDialog(null,e);}
    tbl1.setModel(modelo);
    
    }
    
   
    public static String getAño(){
    Date data= new Date();
    SimpleDateFormat formato= new SimpleDateFormat("yyy");
    return formato.format(data);
    
    }
    public static String getMes(){
    Date date= new Date();
    SimpleDateFormat formato= new SimpleDateFormat("MM");
    return formato.format(date);
    }
    
    public static String getDia(){
    Date date= new Date();
    SimpleDateFormat formato= new SimpleDateFormat("dd");
    return formato.format(date);
    }
    public void fecha(){
    txaño.setText(getAño());
    txmes.setText(getMes());
    txdia.setText(getDia());
    }
    
    public void img(){
        /**
  ImageIcon nd= (ImageIcon) lblfoto.getIcon();
  Image img = nd.getImage();
  Image newimg = img.getScaledInstance(170, 170, java.awt.Image.SCALE_SMOOTH);
  ImageIcon newicon = new ImageIcon(newimg);
  lblfoto.setIcon(newicon); 
  *
              */
     
         ImageIcon img2 =  new ImageIcon(getClass().getResource("/org/josueroldan/ico/car1.jpg"));
         Image img = img2.getImage();
         Image newimg = img.getScaledInstance(214,182, java.awt.Image.SCALE_SMOOTH);
         ImageIcon newicon = new ImageIcon(newimg);
         lblfoto.setIcon(newicon); 
         
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        btnmodificar = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        btnAgregarImagen = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        btmodificar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lblfoto = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        txnmid = new javax.swing.JTextField();
        txtcodigo = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txaño = new javax.swing.JTextField();
        txmes = new javax.swing.JTextField();
        txdia = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txcategoria = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        btnConsultar = new javax.swing.JButton();
        txn = new javax.swing.JTextField();
        cbox = new javax.swing.JComboBox<>();
        btactualizar = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        btnmodificar.setText("Modificar");
        btnmodificar.setToolTipText("");
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(btnmodificar);

        jMenuItem2.setText("Eliminar");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jPopupMenu1.add(jMenuItem2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setUndecorated(true);

        tbl1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tbl1.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(tbl1);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        btnAgregarImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/josueroldan/ico/1398203193_social_6.png"))); // NOI18N
        btnAgregarImagen.setText("Imagen");
        btnAgregarImagen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarImagenActionPerformed(evt);
            }
        });

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/josueroldan/ico/Save all.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/josueroldan/ico/tab_new_raised1.png"))); // NOI18N
        jButton1.setText("Nuevo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/josueroldan/ico/Cancel Button.png"))); // NOI18N
        jButton2.setText("Cancelar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        btmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/josueroldan/ico/configuration_settings.png"))); // NOI18N
        btmodificar.setText("Modificar");
        btmodificar.setEnabled(false);
        btmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btmodificarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAgregarImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(btmodificar)
                    .addComponent(btnAgregarImagen))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));

        lblfoto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblfoto, javax.swing.GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblfoto, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(0, 0, 0));

        txnmid.setEditable(false);
        txnmid.setFont(new java.awt.Font("Dialog", 0, 3)); // NOI18N
        txnmid.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txtcodigo.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        txtcodigo.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtcodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcodigoKeyTyped(evt);
            }
        });

        txtnombre.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        txtnombre.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Alto:");

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Ancho:");

        txaño.setEditable(false);
        txaño.setFont(new java.awt.Font("Dialog", 0, 5)); // NOI18N
        txaño.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txmes.setEditable(false);
        txmes.setFont(new java.awt.Font("Dialog", 0, 5)); // NOI18N
        txmes.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txdia.setEditable(false);
        txdia.setFont(new java.awt.Font("Dialog", 0, 5)); // NOI18N
        txdia.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Tipo:");

        txcategoria.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(295, 295, 295)
                        .addComponent(txdia, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txmes, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txaño, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txnmid, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtnombre)
                                    .addComponent(txtcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 384, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(13, 13, 13)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txcategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(txnmid, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txaño, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txmes, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txdia, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(50, 50, 50)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txcategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(0, 0, 0));

        btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/josueroldan/ico/Zoom.png"))); // NOI18N
        btnConsultar.setText("Consultar");
        btnConsultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarActionPerformed(evt);
            }
        });

        txn.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        cbox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CODIGO" }));

        btactualizar.setText("Actualizar");
        btactualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btactualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(cbox, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txn, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btactualizar, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnConsultar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConsultar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btactualizar))
                .addContainerGap())
        );

        jMenu1.setText("Salir");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu1MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Info");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu2MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarImagenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarImagenActionPerformed
        lblfoto.setIcon(null);
        JFileChooser j=new JFileChooser();
        j.setFileSelectionMode(JFileChooser.FILES_ONLY);//solo archivos y no carpetas
        int estado=j.showOpenDialog(null);
        if(estado== JFileChooser.APPROVE_OPTION){
            try{
                fis=new FileInputStream(j.getSelectedFile());
               
                File fl= j.getSelectedFile();
                String dirc= fl.getName();
                txtnombre.setText(dirc);
                //necesitamos saber la cantidad de bytes
                this.longitudBytes=(int)j.getSelectedFile().length();
                System.out.print(longitudBytes);
                String d=fl.getAbsolutePath();
                try {
                   
                ImageIcon icono2 = new ImageIcon(d); // esta llamada espera a que la imagen esté cargada.
                Integer alto = icono2.getIconHeight();
                Integer ancho=icono2.getIconWidth();
                txtcodigo.setText(ancho.toString());
                txtnombre.setText(alto.toString());
                if(d.endsWith(".png")){
                txcategoria.setText("image/png");
                }
                else
                if(d.endsWith(".jpg")){
                txcategoria.setText("image/jpeg");
                
                }
                    Image icono=ImageIO.read(j.getSelectedFile()).getScaledInstance
                                    
                            (lblfoto.getWidth(),lblfoto.getHeight(),Image.SCALE_DEFAULT);
              
                
                   
                    lblfoto.setIcon(new ImageIcon(icono));
                    lblfoto.updateUI();
                 

                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(rootPane, "imagen: "+ex);
                }
            }catch(FileNotFoundException ex){
                ex.printStackTrace();
            }
        }        // TODO add your handling code here:
    }//GEN-LAST:event_btnAgregarImagenActionPerformed

    private void btnConsultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarActionPerformed
      
  
        if(cbox.getSelectedItem().toString().equalsIgnoreCase("CODIGO")){
        String a= txn.getText();
         buscarcodigo(a);
        }else 
            if(cbox.getSelectedItem().toString().equalsIgnoreCase("NOMBRE")){
         String b= txn.getText();
         buscarnombre(b);
        }
        
        
    }//GEN-LAST:event_btnConsultarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        
        try{
            Conexion mysql= new Conexion();
            Connection cn= (Connection) mysql.conectar();
            String sql="INSERT INTO imagephp(anchura,altura, tipo,imagen) VALUES (?,?,?,?)";

            PreparedStatement ps=cn.prepareStatement(sql);
            ps.setInt(1,Integer.parseInt(txtcodigo.getText()));
            ps.setInt(2,Integer.parseInt(txtnombre.getText()));
            ps.setString(3,(txcategoria.getText()));
         
            ps.setBinaryStream(4,fis,longitudBytes);
     
            ps.execute();
            ps.close();

            lblfoto.setIcon(null);
            txtcodigo.setText("");
            txtnombre.setText("");
            mostrardatos();
            JOptionPane.showMessageDialog(rootPane,"Guardado correctamente");
        }catch(SQLException | NumberFormatException | HeadlessException x){
            JOptionPane.showMessageDialog(rootPane, "exception 2 "+x);
        }           // TODO add your handling code here:
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void jMenu1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MousePressed
    System.exit(0);   // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1MousePressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
      nuevo();  // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
   btnAgregarImagen.setEnabled(true);
   Integer c= tbl1.getSelectedRow();

  
   
   String ab= (String) tbl1.getValueAt(c, 0);

 
 String sql="select id,imagen,anchura,altura,tipo from imagephp where id = "+ab;
        ImageIcon foto;
        InputStream is;
        String codigo;
        String nombre;
        String tipo;
        String id;
             Conexion mysql= new Conexion();
            Connection cn= (Connection) mysql.conectar();
        try{
           Statement st= cn.createStatement();
           ResultSet rs= st.executeQuery(sql);
            while(rs.next()){
                id= rs.getString(1);
                is = rs.getBinaryStream(2);
                codigo= rs.getString(3);
                nombre = rs.getString(4);
                tipo=rs.getString(5);
                BufferedImage bi = ImageIO.read(is);
                foto = new ImageIcon(bi);
                
                Image img = foto.getImage();
                Image newimg = img.getScaledInstance(214,182, java.awt.Image.SCALE_SMOOTH);
                
                ImageIcon newicon = new ImageIcon(newimg);
                txnmid.setText(id);
                lblfoto.setIcon(newicon);
                txtcodigo.setText(codigo);
                txtnombre.setText(nombre);
                txcategoria.setText(tipo);
                btnGuardar.setEnabled(false);
                btmodificar.setEnabled(true);
                    txtcodigo.setEnabled(true);
    txtnombre.setEnabled(true);
    lblfoto.setEnabled(true);
            }
        }catch(Exception ex){
            JOptionPane.showMessageDialog(rootPane,"exception: "+ex);
        }

// TODO add your handling code here:
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
       cancelar(); // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
       
        Integer c= tbl1.getSelectedRow();
        String op= (String) tbl1.getValueAt(c, 0);
 
           try{
            Conexion mysql= new Conexion();
            Connection cn= (Connection) mysql.conectar();
            String sql="DELETE FROM imagephp WHERE id="+op;

            PreparedStatement ps=cn.prepareStatement(sql);
      
            ps.execute();
            ps.close();

            mostrardatos();
            JOptionPane.showMessageDialog(rootPane,"Eliminado correctamente");
        }catch(SQLException | NumberFormatException | HeadlessException x){
            JOptionPane.showMessageDialog(rootPane, "exception 2 "+x);
        } 
// TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void btmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btmodificarActionPerformed
       
   
   String s= txnmid.getText();
   Integer r= Integer.parseInt(s);
   String sql="UPDATE  imagephp SET anchura=?, altura=?, imagen=? , tipo=? "+" where id = "+r;

     
   try{
            Conexion mysql= new Conexion();
            Connection cn= (Connection) mysql.conectar();
           

            PreparedStatement ps=cn.prepareStatement(sql);
            ps.setInt(1,Integer.parseInt(txtcodigo.getText()));
            ps.setInt(2,Integer.parseInt(txtnombre.getText()));
            ps.setBinaryStream(3,fis,longitudBytes);
            ps.setString(4,txcategoria.getText());
            
            ps.execute();
            ps.close();

            lblfoto.setIcon(null);
            txtcodigo.setText("");
            txtnombre.setText("");
            mostrardatos();
            cancelar();
            JOptionPane.showMessageDialog(rootPane,"Modificado correctamente");
        }catch(SQLException | NumberFormatException | HeadlessException x){
            JOptionPane.showMessageDialog(rootPane, "exception 2 "+x);
        } 
           // TODO add your handling code here:
    }//GEN-LAST:event_btmodificarActionPerformed

    private void txtcodigoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcodigoKeyTyped
        int k = (int) evt.getKeyChar();
        if (k > 58 && k < 255) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }   
            int d = (int) evt.getKeyChar();
        if (d > 32 && d < 47) {//Si el caracter ingresado es una letra
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);//Limpiar el caracter ingresado
            JOptionPane.showMessageDialog(null, "Solo puede Ingresar numeros!!!", "Validando Datos",
                JOptionPane.ERROR_MESSAGE);

        }    // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoKeyTyped

    private void btactualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btactualizarActionPerformed
     mostrardatos();   // TODO add your handling code here:
    }//GEN-LAST:event_btactualizarActionPerformed

    private void jMenu2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MousePressed
      new panelinfo().setVisible(true);   // TODO add your handling code here:
    }//GEN-LAST:event_jMenu2MousePressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Form1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Form1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Form1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Form1.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Form1().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btactualizar;
    private javax.swing.JButton btmodificar;
    private javax.swing.JButton btnAgregarImagen;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JMenuItem btnmodificar;
    private javax.swing.JComboBox<String> cbox;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblfoto;
    private javax.swing.JTable tbl1;
    private javax.swing.JTextField txaño;
    private javax.swing.JTextField txcategoria;
    private javax.swing.JTextField txdia;
    private javax.swing.JTextField txmes;
    private javax.swing.JTextField txn;
    private javax.swing.JTextField txnmid;
    private javax.swing.JTextField txtcodigo;
    private javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
